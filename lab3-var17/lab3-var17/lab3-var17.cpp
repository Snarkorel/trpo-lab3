/*
3, 17
��� ������������� ������. ��������� ������� transform �������� �� �������������� �����������
��� �������� �������, ���������� ������� ����������

1) ��������� ������ � ������������ � ������� ��������. ��� ���������� ������ ������������ ������-���������.
2) ��������� ���������� ������ �������� �������, �� � �������������� �������.
3) ��������� ���������� ������ ������� � �������, �� � �������������� ����������������� �������
*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void fill(vector<int> &vect)
{
	for (auto i = 0; i <= 64; i++)
	{
		vect.push_back(i);
	}
}

void print(vector<int> vect, char* message)
{
	cout << "\r\n" << message << ":\r\n";
	for each (auto var in vect)
	{
		cout << var << " ";
	}
	cout << "\r\n";
}

int main()
{
	vector<int> initial;
	vector<int> result;

	double scalingCoeff = 3.76;

	fill(initial);
	print(initial, "Inital vector");

	result.resize(initial.size());

	transform(initial.begin(), initial.end(), result.begin(), [scalingCoeff](auto val) 
	{
		auto root = sqrt(val);
		if (root == (int)root)
		{
			return (int)(val * scalingCoeff + .5); //.5 is for proper value rounding while typecast
		}
		return val;
	});

	print(result, "Transformed vector");

	return 0;
}

